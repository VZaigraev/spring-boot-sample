package simple.app.controllers;

import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SimpleController {

    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String helloWorld() {
        return "hello world";
    }
}
